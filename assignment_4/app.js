"use strict";

/**
 * Assignment 4 - Node HTTP Server
 */

const HTTP = require("http");
const PORT = 5000;

/**
 * HTTP endpoints
 */
HTTP.createServer((req, res) => {
  /**
   * Health check endpoint
   */
  if (req.url == "/health") {
    let healthResponse = {
      service: "Health Check Service",
      version: "1.0.0"
    };

    res.writeHead(200, {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(JSON.stringify(healthResponse))
    });
    res.end(JSON.stringify(healthResponse));

    /**
     * Greet endpoint
     * Takes a name
     * Returns a string of "hello" + the name entered
     */
  } else if (req.url.startsWith("/greet")) {
    let myArray = req.url.split("/");

    if (myArray.length == 3) {
      let name = myArray[2];
      let greetString = "hello " + name;

      res.writeHead(200, {
        "Content-Type": "text/plain",
        "Content-Length": Buffer.byteLength(greetString)
      });
      res.end(greetString);
    }

    /**
     * Sum endpoint
     * Takes two integers and adds them together
     * Returns the first and second integers and their sum as a json object
     */
  } else if (req.url.startsWith("/sum")) {
    let numArray = req.url.split("/");

    if (numArray.length == 4) {
      //checks to see if the parameters entered are numbers
      if (isNaN(numArray[2]) || isNaN(numArray[3])) {
        res.writeHead(400);
        res.end("Did not enter a number.");
      } else {
        let nums = {
          first: parseInt(numArray[2]),
          second: parseInt(numArray[3]),
          sum: parseInt(numArray[2]) + parseInt(numArray[3])
        };

        res.writeHead(200, {
          "Content-Type": "application/json",
          "Content-Length": Buffer.byteLength(JSON.stringify(nums))
        });
        res.end(JSON.stringify(nums));
      }
    }

    /**
     * Handles any errors if an incorrect url is entered
     */
  } else {
    res.writeHead(400);
    res.end("No such endpoint. Incorrect url.");
  }
}).listen(PORT, () => {
  console.log("Assignment 4 started on server " + PORT);
});
