"use strict";

/**
 * Assignment 5 - Song Service
 * Gets a list of all active songs
 * Gets all active and inactive songs
 * Gets a song by ID
 *
 * Entry point to the app
 */

const EXPRESS = require("express");
const APP = EXPRESS();
const PORT = 5000;

let healthRoute = require("./routes/healthRoute");
let songsRoute = require("./routes/songsRoute");

APP.use("/health", healthRoute);
APP.use("/songs", songsRoute);

APP.listen(PORT, () =>
  console.log("Welcome to my Express App started on port " + PORT)
);
