"use strict";

const _ = require("lodash");

var songArray = require("../models/songsModel");

/**
 * Returns all active and inactive songs
 */
let returnAllSongs = () => songArray;

/**
 * Returns only active songs
 */
let returnFilteredSongs = () => _.filter(songArray, { active: true });

/**
 * Returns a song by ID
 * @param {int} id
 */
let returnSongByID = id => _.find(songArray, { id: id });

module.exports.returnAllSongs = returnAllSongs;
module.exports.returnFilteredSongs = returnFilteredSongs;
module.exports.returnSongByID = returnSongByID;
