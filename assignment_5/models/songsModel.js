"use strict";

let songArray = [
  {
    id: 1,
    name: "White Christmas",
    artist: "Bing Crosby",
    active: true
  },
  {
    id: 2,
    name: "Last Christmas",
    artist: "Wham!",
    active: true
  },
  {
    id: 3,
    name: "The Christmas Song",
    artist: "Nat King Cole",
    active: false
  }
];

module.exports = songArray;
