"use strict";

const EXPRESS = require("express");
const ROUTER = EXPRESS.Router();

ROUTER.get("/", (req, res) => {
  res.json({
    name: "Song Service",
    version: "1.0.0"
  });
});

module.exports = ROUTER;
