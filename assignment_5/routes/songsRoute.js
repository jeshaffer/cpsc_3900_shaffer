"use strict";

const EXPRESS = require("express");
const ROUTER = EXPRESS.Router();
var songsController = require("../controllers/songsController");

/**
 * Return active songs
 * http://localhost:5000/songs
 * Query Parameter: ?inactive=true
 */
ROUTER.get("/", (req, res) => {
  let inactive = req.query.inactive == "true";
  if (inactive) {
    //returns all active and inactive songs
    res.json(songsController.returnAllSongs());
    res.status(200);
  } else {
    //only returns active songs
    res.json(songsController.returnFilteredSongs());
    res.status(200);
  }
});

/**
 * GET song by ID
 * http://localhost:5001/songs/{songId}
 * id: ID of a specific song
 */
ROUTER.get("/:id", (req, res) => {
  let id = req.params.id;

  if (!id || isNaN(id)) {
    res.status(400);
    res.json({ message: "Invalid ID parameter" });
  } else {
    let song = songsController.returnSongByID(parseInt(req.params.id));
    if (!song) {
      res.status(404);
      res.json({ message: "No song found with ID: " + id });
    } else {
      res.json(song);
    }
  }
});

module.exports = ROUTER;
