"use strict";
/**
 * Assignment 6 - Complete Express Web Service
 *
 * Entry point to the app
 */

const EXPRESS = require("express");
const APP = EXPRESS();
const PORT = 5000;
const bodyParser = require("body-parser");

let healthRoute = require("./routes/healthRoute");
let booksRoute = require("./routes/booksRoute");

APP.use(bodyParser.json());

APP.use("/health", healthRoute);
APP.use("/books", booksRoute);

APP.listen(PORT, () =>
  console.log("Welcome to my Express App started on port " + PORT)
);
