"use strict";

const _ = require("lodash");
const uuidv4 = require("uuid/v4");

var booksArray = require("../models/booksModel");

/**
 * Returns all books
 */
let returnBooks = () => booksArray;

/**
 * Returns a book by ID
 * @param {guid} id - guid string
 */
let returnBookByID = id => _.find(booksArray, { id: id });

/**
 * Posts book to model
 * @param {guid} id - guid string
 * @param {string} bookName
 * @param {string} author
 * @param {string} genre
 * @param {int} ageInYears
 * @returns book json
 */
let postBookToModel = (id, bookName, author, genre, ageInYears) => {
  let postJson = {
    id: uuidv4(),
    bookName: bookName,
    author: author,
    genre: genre,
    ageInYears: ageInYears
  };

  //array.push(object) will add your json object to your model
  booksArray.push(postJson);

  //returning the json object so that the post can return it validly
  return postJson;
};

/**
 * Return book index by id (int
 * @param {guid} id
 */
let getBookIndex = id => _.findIndex(booksArray, { id: id });

/**
 * Return book object from booksModel
 * @param index - 0 based integer
 */
let getBookObjectByIndex = index => booksArray[index];

/**
 * Replaces book in booksArray
 * @param {int} index
 * @param {*} updatedBook - json
 */
let replaceBookInArray = (index, updatedBook) => {
  booksArray.splice(index, 1, updatedBook);
  return updatedBook;
};

/**
 * Delete a book by id
 * @param {guid} id
 * @return array of removed items
 */
let deleteBookInArray = id => _.remove(booksArray, book => book.id == id);

module.exports.returnBooks = returnBooks;
module.exports.returnBookByID = returnBookByID;
module.exports.postBookToModel = postBookToModel;
module.exports.getBookIndex = getBookIndex;
module.exports.getBookObjectByIndex = getBookObjectByIndex;
module.exports.replaceBookInArray = replaceBookInArray;
module.exports.deleteBookInArray = deleteBookInArray;
