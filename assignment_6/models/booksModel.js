"use strict";

const uuidv4 = require("uuid/v4");

let bookArray = [
  {
    id: uuidv4(),
    bookName: "Harry Potter and the Philosopher's Stone",
    author: "J. K. Rowling",
    genre: "Fantasy Fiction",
    ageInYears: 21
  },
  {
    id: uuidv4(),
    bookName: "Harry Potter and the Chamber of Secrets",
    author: "J. K. Rowling",
    genre: "Fantasy Fiction",
    ageInYears: 20
  },
  {
    id: uuidv4(),
    bookName: "Harry Potter and the Prisoner of Azkaban",
    author: "J. K. Rowling",
    genre: "Fantasy Fiction",
    ageInYears: 19
  },
  {
    id: uuidv4(),
    bookName: "Harry Potter and the Goblet of Fire",
    author: "J. K. Rowling",
    genre: "Fantasy Fiction",
    ageInYears: 18
  },
  {
    id: uuidv4(),
    bookName: "Harry Potter and the Order of the Phoenix",
    author: "J. K. Rowling",
    genre: "Fantasy Fiction",
    ageInYears: 15
  }
];

module.exports = bookArray;
