"use strict";

const EXPRESS = require("express");
const ROUTER = EXPRESS.Router();

var booksController = require("../controllers/booksController");

/**
 * Return all books
 * http://localhost:5000/books
 */
ROUTER.get("/", (req, res) => {
  res.json(booksController.returnBooks());
  res.status(200);
});

/**
 * GET book by ID
 * http://localhost:5000/books/{bookID}
 * id: ID of a specific song as an int
 */
ROUTER.get("/:id", (req, res) => {
  let id = req.params.id;

  let book = booksController.returnBookByID(req.params.id);
  if (!book) {
    res.status(404);
    res.json({ message: "No book found with ID: " + id });
  } else {
    res.json(book);
  }
});

/**
 * POST a book
 * http://localhost:5000/books
 * req body: {id: int, bookName: "string", author: "string", genre: "string", ageInYears: int}
 */
ROUTER.post("/", (req, res) => {
  let id = req.body.id;
  let bookName = req.body.bookName;
  let author = req.body.author;
  let genre = req.body.genre;
  let ageInYears = req.body.ageInYears;

  //verify that the variables are valid
  if (!id || !ageInYears) {
    res.status(400);
    res.json({
      error: "Invalid or non-numeric field was passed in with the json"
    });
  } else if (!bookName || !author || !genre) {
    res.status(400);
    res.json({
      error: "Invalid or no field was passed in with the json"
    });
  } else {
    let returnBook = booksController.postBookToModel(
      id,
      bookName,
      author,
      genre,
      ageInYears
    );
    res.json(returnBook);
  }
});

/**
 * UPDATE for /books route
 * Requires id param - String GUID
 * Returns updated book
 * req body: {id: int, bookName: "string", author: "string", genre: "string", ageInYears: int}
 */
ROUTER.put("/:id", (req, res) => {
  let id = req.params.id;
  let bookName = req.body.bookName;
  let author = req.body.author;
  let genre = req.body.genre;
  let ageInYears = req.body.ageInYears;

  if (!id) {
    res.status(404);
    res.json({
      error: "Invalid ID"
    });
  } else if (ageInYears && isNaN(ageInYears)) {
    res.status(400);
    res.json({
      error: "Invalid ageInYears"
    });
  } else {
    let bookIndex = booksController.getBookIndex(id);

    if (bookIndex < 0) {
      res.status(404);
      res.json({ error: "Book entry not found with id: " + id });
    } else {
      //retrieving object to update
      let updateBook = booksController.getBookObjectByIndex(bookIndex);

      //updating fields if they have values
      updateBook.bookName = bookName ? bookName : updateBook.bookName;
      updateBook.author = author ? author : updateBook.author;
      updateBook.genre = genre ? genre : updateBook.genre;
      updateBook.ageInYears = ageInYears ? ageInYears : updateBook.ageInYears;

      //updating and returning the data
      let finishedBook = booksController.replaceBookInArray(
        bookIndex,
        updateBook
      );

      res.json(finishedBook);
    }
  }
});

/**
 * DELETE a book by id
 * Requires id param - String GUID
 * http://localhost/books/:id
 */
ROUTER.delete("/:id", (req, res) => {
  let id = req.params.id;

  if (!id && isNaN(id)) {
    res.status(404);
    res.json({ error: "No ID for DELETE or is non-numeric" });
  }

  let removedItems = booksController.deleteBookInArray(id);

  if (removedItems) {
    res.json(removedItems);
  } else {
    res.status(404);
    res.json({ error: "Book not found with id: " + id });
  }
});

module.exports = ROUTER;
