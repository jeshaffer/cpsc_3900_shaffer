"use strict";

var CoinFlip = require("../models/CoinFlip");
var CoinFlips = require("../models/CoinFlips");

/**
 * Flips coin and gives heads or tails
 */
let returnSingleCoinFlip = () => {
  let flip = new CoinFlip();

  flip.flipCoin();
  return flip;
};

/**
 * Flips coin the specified number of times
 * GET / coinflip/{integer}?noLimit=true
 * Flips a coin as many times as the integer specified
 * @param numflips - the number of times to flip the coin
 */
function multipleCoinFlips(numflips) {
  let flip = new CoinFlip();
  let singleFlipArray = [];

  let headsTotal = 0;
  let tailsTotal = 0;
  let total = 0;

  for (var i = 1; i <= numflips; i++) {
    flip.flipCoin();
    singleFlipArray.push({
      flipNumber: i,
      side: flip._side,
      coinImage: flip._coinImage
    });

    if (flip._side == "heads") {
      headsTotal++;
    } else {
      tailsTotal++;
    }
    total++;
  }

  let outcome = findOutcome(headsTotal, tailsTotal);
  let coinImage = findCoinImage(outcome);
  let percentage = findPercentage(headsTotal, tailsTotal, total);

  let flips = new CoinFlips(outcome, coinImage, percentage, singleFlipArray);
  return flips;
}

/**
 * find outcome
 * @param {int} hTotal
 * @param {int} tTotal
 * @param {int} total
 */
function findOutcome(hTotal, tTotal) {
  if (hTotal > tTotal) {
    return "heads";
  } else {
    return "tails";
  }
}

/**
 * find coin image
 * @param {string} side
 */
function findCoinImage(side) {
  if (side == "heads") {
    return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvGh5zRsmE29sOcMDl5JWC8S9ZfH1pnhNCxMxhrzYaIARj3u9H";
  } else {
    return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRF_K46xEJM8hvUAcmBh6LpbobXkpLzKthwLxTbeep6f87TmbMl";
  }
}

/**
 * find percentage
 * @param {int} hTotal
 * @param {int} tTotal
 * @param {int} total
 */
function findPercentage(hTotal, tTotal, total) {
  if (hTotal > tTotal) {
    return ((hTotal / total) * 100).toFixed(2);
  } else {
    return ((tTotal / total) * 100).toFixed(2);
  }
}

module.exports.returnSingleCoinFlip = returnSingleCoinFlip;
module.exports.multipleCoinFlips = multipleCoinFlips;
module.exports.findOutcome = findOutcome;
module.exports.findCoinImage = findCoinImage;
module.exports.findPercentage = findPercentage;
