"use strict";

const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.json({
    name: "Coin Flip API",
    version: "1.0.0"
  });
});

module.exports = router;
