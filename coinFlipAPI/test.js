"use strict";

var expect = require("chai").expect;

let findOutcome = require("./controllers/coinController").findOutcome;
let findCoinImage = require("./controllers/coinController").findCoinImage;
let findPercentage = require("./controllers/coinController").findPercentage;

/**
 * Test findOutcome()
 */
describe("findOutcome()", function() {
  it("should return heads or tails depending on the totals of heads/tails", function() {
    let hTotal = 20;
    let tTotal = 19;
    let outcome = "heads";

    let expectedOutcome = findOutcome(hTotal, tTotal);
    expect(expectedOutcome).to.be.equal(outcome);
  });
});

/**
 * Test findCoinImage()
 */
describe("findCoinImage", function() {
  it("should return the correct coinImage", function() {
    let side = "tails";
    let coinImage =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRF_K46xEJM8hvUAcmBh6LpbobXkpLzKthwLxTbeep6f87TmbMl";

    let expectedCoinImage = findCoinImage(side);
    expect(expectedCoinImage).to.be.equal(coinImage);
  });
});

/**
 * Test findPercentage()
 */
describe("findPercentage()", function() {
  it("should find the outcome percentage", function() {
    let hTotal = 6;
    let tTotal = 3;
    let total = 9;
    let percentage = "66.67";

    let expectedPercentage = findPercentage(hTotal, tTotal, total);
    expect(expectedPercentage).to.equal(percentage);
  });
});
