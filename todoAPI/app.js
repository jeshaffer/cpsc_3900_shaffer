"use strict";

const express = require("express");
const app = express();
const port = 5000;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const LOGGER = require("morgan");

app.use(LOGGER("dev"));
app.use(bodyParser.json());

const todoRoute = require("./routes/todoRouter");
const loginRouter = require("./routes/loginRouter");
const logoutRouter = require("./routes/logoutRouter");

app.use("/todo", todoRoute);
app.use("/login", loginRouter);
app.use("/logout", logoutRouter);

const CONNECT_STRING = require("./connection").connectionString;
mongoose.connect(CONNECT_STRING);

app.listen(port, () => console.log("Welcome to Todo API at port: " + port));
