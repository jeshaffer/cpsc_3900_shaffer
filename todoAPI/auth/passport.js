"use strict";

var passport = require("passport");
var Strategy = require("passport-http-bearer").Strategy;
var userController = require("../controllers/userController");

/**
 * setting up passport to use a bearer strategy
 * -checks mongo for token in system. passes onto middleware
 */

var passportInstance = passport.use(
  new Strategy(function(token, cb) {
    userController
      .findByToken(token)
      .then(user => {
        return cb(null, user);
      })
      .catch(err => {
        return cb(err);
      });
  })
);

/**
 * this is the use of the middleware exactly as needed.
 *
 * use:
 * var auth = require('<path>/passport')
 *
 * router.verb('/', auth, (req, res) => {//do stuff})
 */

var passportMiddleware = passportInstance.authenticate("bearer", {
  session: false
});

module.exports = passportMiddleware;
