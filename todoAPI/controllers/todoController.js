"use strict";

let todoModel = require("../models/todoModel");

/**
 * GET all todos
 */
const getTodos = () => {
  return todoModel.find({});
};

/**
 * GET todo by id
 */
let getTodoById = id => {
  return todoModel.findById(id);
};

/**
 * PUT /todo/<guid>
 */
let updateTodo = (id, name, completed, description) => {
  let updateBody = {};

  if (name) {
    updateBody.name = name;
  }
  if (completed) {
    updateBody.completed = completed;
  }
  if (description) {
    updateBody.description = description;
  }

  return todoModel.findByIdAndUpdate(id, updateBody, { new: true });
};

/**
 * POST /todo
 */
let postTodo = (name, completed, description) => {
  let todoRecord = todoModel({
    name: name,
    completed: completed,
    description: description
  });

  return todoRecord.save();
};

/**
 * DELETE /todo/<guid>
 */
let deleteTodo = id => todoModel.findByIdAndDelete(id);

module.exports.getTodos = getTodos;
module.exports.getTodoById = getTodoById;
module.exports.updateTodo = updateTodo;
module.exports.postTodo = postTodo;
module.exports.deleteTodo = deleteTodo;
