"use strict";

const UUID = require("uuid/v4");

let userModel = require("../models/userModel");

/**
 * POST /login
 */
let postLogin = (username, password) => {
  let loginRecord = userModel({
    username: username,
    password: password
  });

  loginRecord.token = UUID();

  return loginRecord.save();
};

/**
 * DELETE /logout
 */
let deleteLogout = user => {
  let response = userModel.findByIdAndUpdate(user._id, { token: undefined });
  return response;
};

/**
 * Find by token
 */
let findByToken = token => {
  let response = userModel.findOne({ token: token });
  return response;
};

module.exports.postLogin = postLogin;
module.exports.deleteLogout = deleteLogout;
module.exports.findByToken = findByToken;
