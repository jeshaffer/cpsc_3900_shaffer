"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let todoSchema = new Schema({
  name: { type: String, required: true },
  completed: { type: Boolean, default: false },
  description: String
});

let todoModel = mongoose.model("todo", todoSchema);
module.exports = todoModel;
