"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let loginSchema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  token: { type: String }
});

let loginModel = mongoose.model("login", loginSchema);
module.exports = loginModel;
