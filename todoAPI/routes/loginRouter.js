"use strict";

const router = require("express").Router();
const loginController = require("../controllers/userController");

/**
 * POST /login
 * requires json body - userName and password
 */
router.post("/", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;

  loginController
    .postLogin(username, password)
    .then(responseLogin => {
      res.json(responseLogin);
    })
    .catch(err => {
      res.status(400);
      res.json(err);
    });
});

module.exports = router;
