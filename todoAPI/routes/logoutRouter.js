"use strict";

const router = require("express").Router();
const userController = require("../controllers/userController");
const auth = require("../auth/passport");
const passport = require("passport");
const Strategy = require("passport-http-bearer");

passport.use(
  new Strategy(function(token, cb) {
    userController
      .findByToken(token)
      .then(user => {
        return cb(null, user);
      })
      .catch(err => {
        return cb(err);
      });
  })
);

/**
 * DELETE /logout/<guid>
 * logouts (deletes record) based on token
 */
router.post("/", auth, (req, res) => {
  let user = req.user;

  userController
    .deleteLogout(user)
    .then(() => {
      res.json({ status: "Logged out successfully" });
    })
    .catch(err => {
      res.status(404);
      res.json(err);
    });
});

module.exports = router;
