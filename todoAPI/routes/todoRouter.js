"use strict";

const router = require("express").Router();
const todoController = require("../controllers/todoController");
const auth = require("../auth/passport");

let loginModel = require("../models/userModel");

const passport = require("passport");
const Strategy = require("passport-http-bearer").Strategy;

/**
 * Setting up authentication middleware
 */
passport.use(
  new Strategy(function(token, cb) {
    let user = loginModel.find(loginModel, { token: token });
    return cb(null, user ? user : false);
  })
);

/**
 * GET /todo
 * get all todos
 */
router.get("/", (req, res) => {
  todoController
    .getTodos()
    .then(todos => {
      res.json(todos);
    })
    .catch(err => {
      res.status(400);
      res.json(err);
    });
});

/**
 * GET /todo/<guid>
 * get todo by _id
 */
router.get("/:id", (req, res) => {
  let id = req.params.id;

  todoController
    .getTodoById(id)
    .then(todo => {
      res.json(todo);
    })
    .catch(err => {
      res.status(404);
      res.json(err);
    });
});

/**
 * PUT /todo/<guid>
 * update a todo based on _id
 */
router.put("/:id", auth, (req, res) => {
  let id = req.params.id;
  let name = req.body.name;
  let completed = req.body.completed;
  let description = req.body.description;

  todoController
    .updateTodo(id, name, completed, description)
    .then(updateResponse => {
      res.json(updateResponse);
    })
    .catch(err => {
      res.status(404);
      res.json(err);
    });
});

/**
 * POST /todo
 * create a todo
 */
router.post("/", auth, (req, res) => {
  let name = req.body.name;
  let completed = req.body.completed;
  let description = req.body.description;

  if (!name) {
    res.status(400);
    res.json({ error: "Must include name" });
  }

  todoController
    .postTodo(name, completed, description)
    .then(responseTodo => {
      res.json(responseTodo);
    })
    .catch(err => {
      res.status(400);
      res.json(err);
    });
});

/**
 * DELETE /todo/<guid>
 * delete a todo based on _id
 */
router.delete("/:id", auth, (req, res) => {
  let id = req.params.id;

  todoController
    .deleteTodo(id)
    .then(deleteResponse => {
      res.json({ status: "Deletion successful for todo with id " + id });
    })
    .catch(err => {
      res.status(404);
      res.json(err);
    });
});

module.exports = router;
