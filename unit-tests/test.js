var expect = require("chai").expect;
var assert = require("chai").assert;

let concatenateWords = require("./app").concatenateWords;
let removeCharacterInString = require("./app").removeCharacterInString;
let addOddNumbers = require("./app").addOddNumbers;

/**
 * Unit test for concatenateWords()
 */
describe("concatenateWords()", function() {
  it("should concatenate two words with an underscore", function() {
    let word1 = "hello";
    let word2 = "world";

    let expected = concatenateWords(word1, word2);
    expect(expected).to.be.equal("hello_world");
  });
  it("should concatenate two numbers with an underscore", function() {
    let output = concatenateWords(5, 6);
    expect(output).to.be.equal("5_6");
  });
});

/**
 * Unit test for removeCharacterInString()
 */
describe("removeCharacterInString()", function() {
  it("should remove a character from a string", function() {
    let inputString = "Hello World";
    let characterToRemove = "H";

    assert.typeOf(inputString, "string");
    let expected = removeCharacterInString(inputString, characterToRemove);
    expect(expected).to.be.equal("ello World");
  });
});

/**
 * Unit test for addOddNumbers()
 */
describe("addOddNumbers()", function() {
  it("should add odd numbers together", function() {
    let expected = addOddNumbers(1, 2, 3, 4);
    expect(expected).to.be.equal(4);
  });

  it("should not add even numbers together", function() {
    let output = addOddNumbers(1, 2, 3, 4);
    expect(output).to.not.equal(6);
  });

  it("should not add all numbers", function() {
    let output = addOddNumbers(5, 6, 7, 8);
    expect(output).to.not.equal(26);
  });
});
